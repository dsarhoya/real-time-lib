(function (root, factory) {
  if (typeof define === "function" && define.amd) {
    // AMD
    // console.log("AMD");
    define([
      "socket.io-client",
      "@feathersjs/feathers",
      "@feathersjs/socketio-client",
    ], factory);
  } else if (typeof exports === "object") {
    // Node, CommonJS-like
    // console.log("CommonJS-like");
    module.exports = factory(
      require("socket.io-client"),
      require("@feathersjs/feathers"),
      require("@feathersjs/socketio-client")
    );
  } else {
    // Browser globals (root is window)
    // console.log("Browser");
    root.returnExports = factory(root.io, root.feathers);
  }
})(this, function (io, feathers, socketio) {
  const socket = io("https://realtime.dsy.cl");
  const client = feathers();
  client.configure(socketio(socket));

  function DynamicData() {
    this.slugCallbacks = {};

    var self = this;
    client.service("dynamic-data").on("patched", function (data) {
      // console.log('patched', data, self.slugCallbacks);
      let c = self.slugCallbacks[data.slug];
      delete data.slug;
      c(data);
    });

    client.io.on("connect", function () {
      console.log("connect", self.slugCallbacks);
      for (const slug in self.slugCallbacks) {
        if (Object.hasOwnProperty.call(self.slugCallbacks, slug)) {
          const callback = self.slugCallbacks[slug];
          self.connect(slug, callback);
        }
      }
    });
  }

  DynamicData.prototype.connect = function (slug, callback) {
    client.service("dynamic-data-connection").create({
      slug: slug,
    });

    this.slugCallbacks[slug] = callback;
  };

  return DynamicData;
});
