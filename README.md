# Browser

```
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/core-js/2.1.4/core.min.js"></script>    
    <script src="//unpkg.com/@feathersjs/client@^4.5.0/dist/feathers.js"></script>
    <script src="//unpkg.com/socket.io-client@^2.3.0/dist/socket.io.js"></script>
    <script src="browser-lib.js"></script>
    <script>
      window.onload = function () {
        var d = new DynamicData();
      
        const addEventListener = (selector, event, handler) => {
          document.addEventListener(event, async ev => {
            handler(ev);
          });
        };
      
        addEventListener('#connect-to-slug', 'submit', async ev => {
          const input = document.querySelector('[id="slug"]');
      
          ev.preventDefault();
      
      
          d.connect(input.value, function (data) {
            console.log(input.value, data);
          });
      
          input.value = '';
        });
      };
    </script>
```

# NODE

```
{
  "dependencies": {
    "@feathersjs/feathers": "^4.5.11",
    "@feathersjs/socketio-client": "^4.5.11",
    "socket.io-client": "^2.3",
    "dsy-real-time-lib": "ssh://git@bitbucket.org:dsarhoya/real-time-lib.git#0.0.1",
  },
}
```

```term
yarn add @feathersjs/feathers @feathersjs/socketio-client socket.io-client@^2.3
yarn add ssh://git@bitbucket.org:dsarhoya/real-time-lib.git#0.0.1
```
