(function (window, io, feathers) {
    "use strict";
  
    const socket = io('https://realtime.dsy.cl');
    const client = feathers();
    client.configure(feathers.socketio(socket));
  
    window.DynamicData = function () {
      this.slugCallbacks = {};
  
      var self = this;
      client.service('dynamic-data').on('patched', function (data) {
        // console.log('patched', data, self.slugCallbacks);
        let c = self.slugCallbacks[data.slug];
        delete data.slug;
        c(data);
  
      });
  
      client.io.on('connect', function () {
        console.log('connect', self.slugCallbacks);
        for (const slug in self.slugCallbacks) {
          if (Object.hasOwnProperty.call(self.slugCallbacks, slug)) {
            const callback = self.slugCallbacks[slug];
            self.connect(slug, callback);
          }
        }
      });
    };
  
    window.DynamicData.prototype.connect = function (slug, callback) {
      client.service('dynamic-data-connection').create({
        slug: slug
      });
  
      this.slugCallbacks[slug] = callback;
    };
  
  })(window, io, feathers);
  